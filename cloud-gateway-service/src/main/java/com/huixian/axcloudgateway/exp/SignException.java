package com.huixian.axcloudgateway.exp;


import com.cloud.common.exception.BusinessException;
import com.cloud.common.model.IError;

/**
 * @author kkyv
 * @Date 2020/9/1 8:05 PM
 * @Created by kkyv
 */
public class SignException extends BusinessException {

    private static final long serialVersionUID = -2235415786280157610L;

    public SignException(IError iError) {
        super(iError);
    }
}
