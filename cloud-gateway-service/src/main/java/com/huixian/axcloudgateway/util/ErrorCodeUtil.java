package com.huixian.axcloudgateway.util;

import com.huixian.axcloudgateway.enums.BizErrorEnum;
import org.apache.commons.lang3.StringUtils;

/**
 * Description :   .
 *
 * @author : LuShunNeng
 * @date : Created in 2020-05-27 18:10
 */
public class ErrorCodeUtil {

    /**
     * 构建错误码
     * @param bizErrorEnum
     * @return
     */
    public static int buildErrorCode(BizErrorEnum bizErrorEnum) {
        return Integer.valueOf(StringUtils
                .join(bizErrorEnum.getSystemNum(),
                        bizErrorEnum.getLayerNum(),
                        bizErrorEnum.getBaseCode()));
    }


}
