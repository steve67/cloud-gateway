package com.huixian.axcloudgateway.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ParamStringUtils {

	/**
	 * 拼接 JSONObject 类型的参数
	 * @param requestObject 请求类型
	 * @return 拼接字符串
	 */
	public static String concatJSONObjectParam(JSONObject requestObject) {
		List<String> paramPair = new ArrayList<>();
		requestObject.keySet().stream().sorted().forEach((String e) -> {
			//排除这两个key
			if ((!e.equals("sign")) && (!e.equals("signType"))) {
				Object rObj = requestObject.get(e);
				//为null不处理
				if (rObj != null) {
					if (rObj instanceof JSONObject) {
						paramPair.add(e + "={" + concatJSONObjectParam((JSONObject) rObj) + "}");
					} else if (rObj instanceof JSONArray) {
						paramPair.add(e + "=" + concatJSONArrayParam((JSONArray) rObj));
					} else {
						paramPair.add(e + "=" + rObj.toString());
					}
				}
			}
		});
		return StringUtils.join(paramPair, "&");
	}
	/**
	 * 拼接 JSONArray 类型的参数
	 * @param array 请求类型
	 * @return 拼接字符串
	 */
	public static String concatJSONArrayParam(JSONArray array) {
		List<String> arrList = new ArrayList<>();
		array.forEach(o -> {
			if (o == null) {
				arrList.add("");
			}
			if (o instanceof JSONObject) {
				arrList.add("{" + concatJSONObjectParam((JSONObject) o) + "}");
			} else if (o instanceof JSONArray) {
				arrList.add(concatJSONArrayParam((JSONArray) o));
			} else {
				arrList.add(o.toString());
			}
		});
		return "[" + StringUtils.join(arrList, ",") + "]";
	}


}
