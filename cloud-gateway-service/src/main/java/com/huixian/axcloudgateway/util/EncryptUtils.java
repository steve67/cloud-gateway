package com.huixian.axcloudgateway.util;

import com.cloud.common.exception.BusinessException;
import com.huixian.axcloudgateway.enums.BizErrorEnum;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Base64Utils;

/**
 * Description :   .
 *
 * @author : maoyanjia
 * @date : Created in 2020/6/10 15:59
 */
@Slf4j
public class EncryptUtils {

    public static byte[] commonEncrypt(byte[] data) {
        try {
            byte[] uncompressBytes = uncompress(data);
            return Base64Utils.decode(uncompressBytes);
        }catch (Exception e) {
            log.error("数据解析异常：{}", Arrays.toString(data));
            throw new BusinessException(BizErrorEnum.DATA_VALID_ERROR, e.getMessage());
        }

    }

    public static byte[] commonUrlEncrypt(byte[] data) {
        try {
            byte[] decode = Base64Utils.decode(data);
            byte[] uncompress = uncompress(decode);
            return URLDecoder.decode(new String(uncompress), "UTF-8").getBytes();
        }catch (Exception e) {
            log.error("数据解析异常：{}", Arrays.toString(data), e);
            throw new BusinessException(BizErrorEnum.DATA_VALID_ERROR, e.getMessage());
        }

    }


    public static byte[] byteEncrypt(byte[] data) {

        byte[] uncompressBytes = uncompress(data);

        int orKeyIndex = uncompressBytes.length / 3;
        byte orKeyResult = 0x00;

        byte[] realBodyBytes = new byte[uncompressBytes.length -1];
        int k = 0;

        for(int i = 0; i< uncompressBytes.length ; i++) {
            if (orKeyIndex == i) {
                orKeyResult = uncompressBytes[i];
            } else {
                if (realBodyBytes.length >= i) {
                    realBodyBytes[k] = uncompressBytes[i];
                    k++;
                }
            }
        }

        realBodyBytes = Base64Utils.decode(realBodyBytes);
        byte[] encrypt = encrypt(realBodyBytes, orKeyResult);
        return Base64Utils.decode(encrypt);
    }

    public static byte[] encrypt(byte[] bytes, byte orKey) {
        if (bytes == null) {
            return null;
        }
        int len = bytes.length;
        for (int i = 0; i < len; i++) {
            bytes[i] ^= orKey;
        }
        return bytes;
    }


    public static byte[] uncompress(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return null;
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        try {
            GZIPInputStream ungzip = new GZIPInputStream(in);
            byte[] buffer = new byte[1024];
            int n;
            while ((n = ungzip.read(buffer)) >= 0) {
                out.write(buffer, 0, n);
            }
            ungzip.close();
            in.close();
        } catch (Exception e) {
//            e.printStackTrace();
            throw new BusinessException(e);
        }
        return out.toByteArray();
    }
}
