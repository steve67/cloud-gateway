package com.huixian.axcloudgateway.filter;

import com.cloud.common.enums.RequestContextEnum;
import com.huixian.axcloudgateway.enums.FilterConfig;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

/**
 * 全局过滤器，设置http请求头的traceId和网关的traceId
 *
 * @author
 */
@Slf4j
@Component
public class TraceIdGlobalFilter implements GlobalFilter, Ordered {

    /**
     * 执行逻辑
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String traceId = getTraceId(exchange);

        // set MDC, need remove
        MDC.put(RequestContextEnum.TRACE_ID.name(), traceId);

        long startTime = System.currentTimeMillis();
        log.info("uri: {}", exchange.getRequest().getURI().toString());
        log.info("info traceId :{}", traceId);
        // 向http request 中添加请求头RequestContextEnum.TRACE_ID
        ServerHttpRequest request = exchange.getRequest().mutate()
                .header(RequestContextEnum.TRACE_ID.getOutCode(), traceId)
                .build();


        return chain.filter(exchange.mutate().request(request).build())
                .then(Mono.fromRunnable(() -> {
                    log.info("请求耗时：{}ms", System.currentTimeMillis() - startTime);
                    MDC.remove(RequestContextEnum.TRACE_ID.name());
                }));
    }

    private String getTraceId(ServerWebExchange exchange) {
        List<String> traceIds = exchange.getRequest().getHeaders()
                .get(RequestContextEnum.TRACE_ID.getOutCode());
        // 从request头中获取，获取不到则（使用UUID）生成一个
        return CollectionUtils.isEmpty(traceIds) ? UUID.randomUUID().toString() : traceIds.get(0);
    }


    /**
     * 设置最高优先级执行 执行顺序 数字越小，执行顺序优先级越高
     */
    @Override
    public int getOrder() {
        return FilterConfig.FilterOrderEnum.TraceIdGlobalFilter.getOrder();
    }

}