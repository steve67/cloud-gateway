package com.huixian.axcloudgateway.logger;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;

/**
 *
 * @author snlu
 * @date 2018/8/20
 */
public class LogBackExEncoder extends PatternLayoutEncoder {

  static {
    PatternLayout.defaultConverterMap.put("T", ThreadNumConverter.class.getName());
  }

}
