package com.huixian.axcloudgateway.aop;

import com.cloud.common.exception.BusinessException;
import com.cloud.common.util.ApiResponseUtil;
import com.huixian.axcloudgateway.enums.BizErrorEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Map;

import static org.springframework.web.reactive.function.server.RequestPredicates.all;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

/**
 * @author zhanglong
 */
@Slf4j
public class GlobalExceptionHandler extends DefaultErrorWebExceptionHandler {

    @Value("${spring.application.name:}")
    private String applicationName;

    public GlobalExceptionHandler(ErrorAttributes errorAttributes, ResourceProperties resourceProperties, ErrorProperties errorProperties, ApplicationContext applicationContext) {
        super(errorAttributes, resourceProperties, errorProperties, applicationContext);
    }

    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(
            ErrorAttributes errorAttributes) {
        return route(acceptsTextHtml(), this::renderErrorResponse).andRoute(all(), this::renderErrorResponse);
    }

    @Override
    protected Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        boolean includeStackTrace = isIncludeStackTrace(request, MediaType.ALL);
        Map<String, Object> error = getErrorAttributes(request, includeStackTrace);
        int httpStatus = getHttpStatus(error);

        Throwable throwable = getError(request);
        log.error("GLOBAL EXCEPTION:{}", request.path(), throwable);
        log.error("info traceId :{}", request.headers());

        int errorCode;
        String errorDesc;

        BusinessException businessException = new BusinessException(BizErrorEnum.SYSTEM_ERROR.getDesc(), throwable);
        errorCode = ApiResponseUtil.getErrorCode(businessException);
        errorDesc = businessException.getMessage();


        return ServerResponse.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(ApiResponseUtil.errorResponse(errorCode, errorDesc)));
    }

    @Override
    public Mono<Void> handle(ServerWebExchange serverWebExchange, Throwable throwable) {
        return super.handle(serverWebExchange, throwable);
    }
}