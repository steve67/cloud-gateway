//package com.huixian.axcloudgateway.config;
//
//import com.ctrip.framework.apollo.model.ConfigChange;
//import com.ctrip.framework.apollo.model.ConfigChangeEvent;
//import com.huixian.apollo.gateway.DynamicRoutingApolloPropertiesChangeEvent;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.gateway.event.EnableBodyCachingEvent;
//import org.springframework.cloud.gateway.filter.AdaptCachedBodyGlobalFilter;
//import org.springframework.stereotype.Component;
//
///**
// * @author zhanglong
// * @date 2020-10-16
// * package com.huixian.axcloudgateway.config
// * <p>
// * no coding, no bug
// */
//@Slf4j
//@Component
//public class ApolloChangeEvent extends DynamicRoutingApolloPropertiesChangeEvent {
//
//    private static final String ID_PATTERN = "spring\\.cloud\\.gateway\\.routes\\[\\d+\\]\\.id";
//
//    @Autowired
//    private AdaptCachedBodyGlobalFilter adaptCachedBodyGlobalFilter;
//
//    @Override
//    public void invoke(ConfigChangeEvent changeEvent) {
//        super.invoke(changeEvent);
//        for (String key : changeEvent.changedKeys()) {
//            ConfigChange change = changeEvent.getChange(key);
//            switch (change.getChangeType()) {
//                case ADDED:
//                    String addKey = change.getPropertyName();
//                    if (addKey.matches(ID_PATTERN)) {
//                        log.info("change adaptCachedBodyGlobalFilter is start, add key:{}", addKey);
//                        String newValue = changeEvent.getChange(addKey).getNewValue();
//                        EnableBodyCachingEvent enableBodyCachingEvent = new EnableBodyCachingEvent(new Object(), newValue);
//                        adaptCachedBodyGlobalFilter.onApplicationEvent(enableBodyCachingEvent);
//                        log.info("change adaptCachedBodyGlobalFilter is end, add key:{}", addKey);
//                    }
//                    break;
//                case MODIFIED:
//                case DELETED:
//                default:
//                    break;
//            }
//        }
//    }
//}
