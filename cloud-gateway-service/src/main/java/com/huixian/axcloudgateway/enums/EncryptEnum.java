package com.huixian.axcloudgateway.enums;

import com.huixian.axcloudgateway.util.EncryptUtils;
import java.util.Objects;
import java.util.function.Function;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Description :   .
 *
 * @author : maoyanjia
 * @date : Created in 2020/6/10 15:57
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum  EncryptEnum {
    BYTE("byte",
            EncryptUtils::byteEncrypt),
    COMMON("common",
            EncryptUtils::commonEncrypt),
    COMMON_URL("common-url",
            EncryptUtils::commonUrlEncrypt),
    ;
    private String type;

    private Function<byte[], byte[]> function;


    public static EncryptEnum getEncryptByType(String type) {
        for (EncryptEnum value : EncryptEnum.values()) {
            if (Objects.equals(value.getType(), type)) {
                return value;
            }
        }
        return null;
    }
}
