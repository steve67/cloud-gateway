package com.huixian.axcloudgateway.enums;

import com.huixian.axcloudgateway.filter.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.core.Ordered;

/**
 * @author zhanglong
 * @date 2020-09-17
 * package com.huixian.axcloudgateway.enums
 * <p>
 * no coding, no bug
 */
@Data
public class FilterConfig {

    @AllArgsConstructor
    @Getter
    public enum FilterOrderEnum {
        /**
         *
         */
        TraceIdGlobalFilter(TraceIdGlobalFilter.class, FilterType.GLOBAL, Ordered.HIGHEST_PRECEDENCE + 1),



        ;
        Class<?> clazz;
        FilterType filterType;

        Integer order;
    }

    @AllArgsConstructor
    @Getter
    public enum FilterType {
        /**
         *
         */
        GLOBAL("全局"),
        ROUTE("路由");

        String desc;
    }

}
