package com.huixian.axcloudgateway.enums;

import com.cloud.common.model.IError;
import com.cloud.common.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * 项目异常枚举.
 *
 * @author : frm
 * @date : Created in 2018/10/18 6:05 PM
 */
public enum BizErrorEnum implements IError {
    /**
     * 项目异常
     */
    SYSTEM_ERROR(999, "系统升级中，请稍后再试"),
    NOT_FOUND_ERROR(404, "地址不存在"),
    SIGN_HEADER_ERROR(1, "鉴权失败，缺少必要的请求头，请检查"),
    TENANT_VALID_ERROR(2, "租户不存在"),
    SIGN_VALID_ERROR(3, "验签失败"),
    HEAD_VALID_ERROR(4, "缺少必要的请求头"),
    DATA_VALID_ERROR(5, "数据校验失败"),

    REQ_PARAM_ERROR(6, "请求参数错误"),
    ;

    private int baseCode;

    private String desc;

    BizErrorEnum(int baseCode, String desc) {
        this.baseCode = baseCode;
        this.desc = desc;
    }

    @Override
    public int getSystemNum() {
        return Integer.valueOf(PropertiesUtil.getProperty("com.huixian.system-num"));
    }

    @Override
    public int getLayerNum() {
        return 1;
    }

    @Override
    public String getBaseCode() {
        return StringUtils.leftPad(String.valueOf(this.baseCode), 3, "0");
    }

    @Override
    public int getCode() {
        return this.baseCode;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
