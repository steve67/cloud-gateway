package com.huixian.autotest;


import com.huixian.axcloudgateway.Application;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Description :   .
 *
 * @author : LuShunNeng
 * @date : Created in 2020-02-01 20:05
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles(value = {"loc"})
@TestPropertySource(locations = "classpath:application-auto.properties")
public class DemoTest {



}
