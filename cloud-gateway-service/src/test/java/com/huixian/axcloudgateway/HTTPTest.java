package com.huixian.axcloudgateway;

import com.alibaba.fastjson.JSONObject;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPOutputStream;

import com.cloud.common.exception.BusinessException;
import com.cloud.common.model.CommonError;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ConnectionPool;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.util.Base64Utils;

/**
 * Description :   .
 *
 * @author : DuanBingfang
 * @date : Created in 2020/7/20 下午 7:10
 */
@Slf4j
public class HTTPTest {

    /**
     * 新建一个Http请求连接
     * 1. 设置连接超时时间为 2s
     * 2. 设置读取超时时间为 4s
     * 3. 设置写入超时时间为 4s
     */
    private static OkHttpClient httpClient;

    /**
     * 静态初始化
     */
    static {
        initHttpClient();
    }

    /**
     * HttpClient初始化
     */
    private static void initHttpClient() {
        httpClient = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(10, 30L, TimeUnit.SECONDS))
                .build();
    }



    /**
     * OkHttp post请求
     *
     * @param url    url链接
     * @param string json格式数据
     *
     * @return .
     */
    public static JSONObject postRequest(String url, String string){
        return postRequest(url, MediaType.parse("application/json; charset=utf-8"), string);
    }

    /**
     * OkHttp post请求
     *
     * @param url       url链接
     * @param mediaType 媒体格式
     * @param string    json格式数据
     *
     * @return .
     */
    public static JSONObject postRequest(String url, MediaType mediaType, String string){
        log.info("postRequest请求参数：string: {}", string);

        RequestBody requestBody = RequestBody.create(mediaType, string);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        return doRequest(request);
    }


    /**
     * 执行请求
     *
     * @param request 请求体
     *
     * @return Json对象
     */
    public static JSONObject doRequest(Request request) {
        if (httpClient == null) {
            initHttpClient();
        }

        log.info("doRequest请求参数：{}", request);

        // 执行请求
        try {
            Response response = httpClient.newCall(request).execute();
            if (response.body() == null) {
                return null;
            }

            String result = response.body().string();
            log.info("doRequest请求结果jsonObject：{}", result);

            return JSONObject.parseObject(result);
        } catch (IOException e) {
            throw new BusinessException(CommonError.SYSTEM_ERROR, "OkHTTP服务超时");
        }
    }

    private static String compress(String str, String encoding) {
        if (str == null || str.length() == 0) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip;
        try {
            gzip = new GZIPOutputStream(out);
            gzip.write(str.getBytes(encoding));
            gzip.close();
        } catch ( Exception e) {
            e.printStackTrace();
        }
        return out.toString();
    }

    public static String compress(String str) throws IOException {
        return compress(str, "UTF-8");

    }

    private static int threadCount = 5; //子线程数

    private static CountDownLatch countDownLatch = new CountDownLatch(threadCount);


    @Data
    static class Mythread implements Runnable {

        private CountDownLatch countDownLatch;

        private String url;

        private String data;

        public Mythread(String url, String data){
            this.url = url;
            this.data = data;
        }

        public Mythread(CountDownLatch countDownLacth){
            this.countDownLatch=countDownLatch;
        }


        @Override
        public void run() {
            JSONObject jsonObject = postRequest(url, data);
            System.out.println(jsonObject);
            countDownLatch.countDown();
        }
    }
    public static void main(String[] args) throws IOException {
        String url = "http://127.0.0.1:7175/saas/sensors-common/data/reportJson";
        BufferedReader fileReader = new BufferedReader(new FileReader(
                new File("C:\\Users\\win7\\Desktop\\test.txt")
        ));
        String line;
        StringBuffer stringBuffer = new StringBuffer();
        while( (line = fileReader.readLine()) != null) {
            stringBuffer.append(line);
        }
        fileReader.close();

        String encode = Base64Utils.encodeToString(stringBuffer.toString().getBytes());
        System.out.println(encode);
        String compress = compress(encode);
/*
        for (int i =0 ; i< 100; i++) {
            JSONObject request = postRequest(url, compress);
            System.out.println(request);
        }
*/

        for(int a=1;a <= threadCount ;a++){
            Mythread mythread = new Mythread(url, compress);
            Thread thread = new Thread(mythread);
            thread.start();
        }
        try{
            countDownLatch.await(); //主线程等待 ,直到countDownLatch 为0
        }catch(Exception e){

        }
    }

}
